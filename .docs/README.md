# SnugComponents / Filter control

## Content

- [Content](#content)
- [Setup](#setup)
- [Usage](#usage)
    * [Injecting/using](#injecting/using)
    * [Own template](#own-template)
    * [Translations](#translations)
    * [Ajax](#ajax)
    * [Laziness](#laziness)
    * [Own FormFactory](#own-formfactory)
    * [Setting](#settings)

## Setup

```neon
extensions:
	SnugcomponentsFilter: Snugcomponents\Filter\FilterExtension
```

## Usage

### Injecting/using

You can simply inject factory in Your Presenters/Controls:

```php
public function __construct(
    private readonly \Snugcomponents\Filter\FilterFactory $filterFactory,
) {
    parent::__construct();
    ...
}
```
And then use it:

```php
public function renderDefault(): void
{
    $this->template->filtered = $this->getComponent('filter')->applyFilter();
}

public function createComponentFilter(): \Snugcomponents\Filter\Filter
{
    /** 
     * At first, you need to specify filter input setup. You need to do this by FilterSetupBuilder. 
     * Every method can be used many times, so you can specify filter only from integers and so on.
     */ 
    $filterSetupBuilder = \Snugcomponents\Filter\FilterSetupBuilder::create()
        ->addInt(name: 'age', caption: 'Your age')
        ->addText(name: 'name', caption: 'Your name')
        ->addFloat(name: 'salary', caption: 'Your salary')
        ->addDatetime(name: 'birth', caption: 'Your birthdate')
        ->addSelect(name: 'superior', caption: 'Your superior', [
            48 => 'Stephen'
            256 => 'Kevin'
        ]);
        
    /**
     * When you are done with setup, then you need to have a class, which implements FilterDataBuilder interface.
     */
    $filterDataBuilder = new class() implements \Snugcomponents\Filter\FilterDataBuilder {
        public function filter(...$filterData): static
        {
            // process filter here and store data for further use (in build function)
            return $this;
        }
    };
        
    // Now you can create the filter component
    return $this->filterFactory->create(
        filterSetupBuilder: $filterSetupBuilder,
        filterDataBuilder: $filterDataBuilder,
        onFilterSubmit: function () {
            // This callback is called when filter form is successfully submitted, and when the filter is cleared
        },
    );
}
```

### Own template

If the original template does not meet your needs, then you can paste your own template into the component by calling `setTemplateFile` method on the `Filter` class instance, which sets the path to the custom `.latte` file to render the filter. For examples, look at the Examples folder.

Your own .latte file should look like this:

```php
{define form}
    {control form}
{/define}
```

`{define form}` is the only required `define` tag. This tag is used by extension.

### Translations

This extension automatically translates all your captions and your own template.
Only thing what you need to do is to register your translator, which implements `Nette\Localization\Translator` interface as a service.
If you need to change default Button caption or clear filter link text, then you need to provide your own template.

### Ajax

This extension fully supports ajax requests. Please feel free to use any js ajax library.

### Laziness

If the filter is empty, then method `filter` from `FilterDataBuilder` interface will not be invoked.

### Own FormFactory

Internally, this component uses his own FormFactory (`Snugcomponents\Filter\BaseFormFactory`). This FormFactory preset
translator and novalidate attribute to filter form. If you need to change this behaviour, you can implement your
version just by implementing `Snugcomponents\Filter\FormFactory` interface and register it in your config.neon:

```neon
services:
	snugcomponents.filter.formFactory: MyOwnBaseFormFactory # needs to implement Snugcomponents\Filter\FormFactory
```

### Settings

Filter needs an instance of the class that implements the `FilterDataBuilder` interface to function. Implement this interface at your discretion. You can find some examples in the `Examples` folder, specifically in the `Examples\Providers` folder.
