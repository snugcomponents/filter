## Installation

To install the latest version of `snugcomponents/filter` use [Composer](https://getcomposer.org).

```bash
composer require snugcomponents/filter
```

## Documentation

For details on how to use this package, check out our [documentation](.docs).

## Versions

| State  | Version  | Branch | Nette | PHP     |
|--------|----------|--------|-------|---------|
| stable | `^2.0.0` | `main` | `4.0` | `>=8.2` |

## Development

This package is currently maintaining by these authors.

<a href="https://gitlab.com/tondajehlar">
  <img alt="TonnyJe" width="80" height="80" src="https://avatars2.githubusercontent.com/u/9120518?v=3&s=80">
</a>

-----

## Conclusion
This package requires PHP 8.2, Nette 4.0, and it is property of SnugDesign © 2022
