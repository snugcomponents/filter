<?php

declare(strict_types=1);

namespace Snugcomponents\Filter;

interface FilterDataBuilder
{
    /**
     * @param float|int|string|array<mixed>|null ...$filterData
     * @return $this
     */
    public function filter(float|int|string|array|null ...$filterData): static;
}
