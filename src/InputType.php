<?php

declare(strict_types=1);

namespace Snugcomponents\Filter;

enum InputType: string
{
    case Text = 'text';
    case Int = 'integer';
    case Float = 'float';
    case Datetime = 'datetime';
    case Select = 'select';
    case CheckboxList = 'checkboxlist';
    case RadioList = 'radiolist';
    case MultiSelect = 'multiple select';
}
