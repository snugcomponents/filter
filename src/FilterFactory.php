<?php

declare(strict_types=1);

namespace Snugcomponents\Filter;

interface FilterFactory
{
    public function create(
        FilterSetupBuilder $filterSetupBuilder,
        FilterDataBuilder $filterDataBuilder,
    ): Filter;
}
