<?php

declare(strict_types=1);

namespace Snugcomponents\Filter;

use Nette\Application\Attributes\Persistent;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Schema\Processor;
use Nette\Schema\ValidationException;
use Nette\Utils\Arrays;

use function array_map;
use function assert;
use function is_array;
use function is_numeric;
use function is_string;
use function str_contains;
use function str_replace;

class Filter extends Control
{
    /** @var array<string, int|float|string|null> */
    #[Persistent] public array $filter = [];

    private string $templateFile = __DIR__ . '/templates/default.latte';

    /** @var array<callable(self): void>  Occurs when page signal is received */
    public array $onFiltering = [];

    public function __construct(
        private readonly FilterSetupBuilder $filterSetupBuilder,
        private readonly FilterDataBuilder $filterDataBuilder,
        private readonly FormFactory $formFactory,
    ) {
    }

    public function render(): void
    {
        $template = $this->getTemplate();
        assert($template instanceof Template);

        $template->render($this->templateFile, [
            'form' => $this->getComponent('form'),
        ]);
    }

    public function createComponentForm(): Form
    {
        $form = $this->formFactory->create();

        $form = $this->filterSetupBuilder->buildForm($form);

        $form->addSubmit('submit');

        $form->onSuccess[] = $this->onSuccess(...); /** @phpstan-ignore-line */

        $form->setDefaults($this->filter);

        return $form;
    }

    public function handleClear(): void
    {
        $this->filter = [];
        Arrays::invoke($this->onFiltering, $this);
        $this->redrawControl('form');
    }

    /**
     * @throws BadRequestException
     */
    public function applyFilter(): FilterDataBuilder
    {
        if (empty($this->filter)) {
            return $this->filterDataBuilder;
        }

        $schema = $this->filterSetupBuilder->getSchema();

        $processor = new Processor();

        $normalized = [];
        try {
            $normalized = $processor->process($schema, array_map($this->stringNumberToNumber(...), $this->filter));
            assert(is_array($normalized));
        } catch (ValidationException) {
            $this->error('Wrong value of filter...');
        }

        return $this->filterDataBuilder->filter(...$normalized);
    }

    public function setTemplateFile(string $templateFile): static
    {
        $this->templateFile = $templateFile;

        return $this;
    }

    /**
     * @param array<string, int|string|float|null> $values
     */
    private function onSuccess(array $values): void
    {
        $this->filter = $values;
        Arrays::invoke($this->onFiltering, $this);
        $this->redrawControl('form');
    }

    /**
     * @param int|float|string|array<mixed>|null $number
     * @return int|float|string|array<mixed>|null
     */
    private function stringNumberToNumber(int|float|string|array|null $number): int|float|string|array|null
    {
        if (!is_string($number)) {
            return $number;
        }

        $otherNumber = str_replace(',', '.', $number);
        if (is_numeric($otherNumber)) {
            if (((string) ((float) $otherNumber)) === $otherNumber && str_contains($otherNumber, '.')) {
                return (float) $otherNumber;
            }

            return (int) $otherNumber;
        }

        return $number;
    }
}
