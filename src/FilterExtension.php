<?php

declare(strict_types=1);

namespace Snugcomponents\Filter;

use Nette\DI\CompilerExtension;

class FilterExtension extends CompilerExtension
{
    public function loadConfiguration(): void
    {
        $this->compiler->loadDefinitionsFromConfig(
            $this->loadFromFile(__DIR__ . '/config/common.neon')['services'],
        );
    }
}
