<?php

declare(strict_types=1);

namespace Snugcomponents\Filter\Exception;

use Exception as PhpException;
use Throwable;

use function sprintf;

class Exception extends PhpException
{
    public function __construct(
        ExceptionCase $case,
        string $variablePart,
        Throwable|null $previous = null,
    ) {
        match ($case) {
            ExceptionCase::InvalidArgumentScalar => parent::__construct(
                sprintf('Invalid argument exception. Scalar (int, float, string or bool), %s given.', $variablePart),
                $case->value,
                $previous,
            ),
        };
    }
}
