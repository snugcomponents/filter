<?php

declare(strict_types=1);

namespace Snugcomponents\Filter\Exception;

enum ExceptionCase: int
{
    case InvalidArgumentScalar = 1;
}
