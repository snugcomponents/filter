<?php

declare(strict_types=1);

namespace Snugcomponents\Filter\Examples\Builders;

use Nette\Database\Table\Selection;
use Nette\SmartObject;
use Snugcomponents\Filter\FilterDataBuilder;

class NetteDatabaseExplorerDataBuilder implements FilterDataBuilder
{
    use SmartObject;

    public function __construct(private Selection $data)
    {
    }

    /**
     * Filter called by the extension
     *
     * @param array<mixed>|float|int|string|null ...$filterData
     */
    public function filter(array|float|int|string|null ...$filterData): static
    {
        // phpcs:ignore SlevomatCodingStandard.Commenting.InlineDocCommentDeclaration.MissingVariable
        /** @var array{age: int|null} $filterData */
        $this->innerFilter(...$filterData); // Because of checking data types.

        return $this;
    }

    /**
     * Build method should be used in all builders.
     * This one only returns the data.
     *
     * In this case you don't need to call filtering at the end in build method.
     * This can be done because Selection is already a builder,
     * which executes query only if some fetch method is called, or it is argument of foreach or count.
     *
     * So this Builder is only Adapter on Selection
     */
    public function build(): Selection
    {
        return $this->data;
    }

    /**
     * All parameters need to be always nullable. It is for filtering only some values...
     * Extension will always pass all the arguments you specified in FilterSetupBuilder
     */
    private function innerFilter(int|null $age): void
    {
        if ($age === null) {
            return;
        }

        $this->data->where('age', $age);
    }
}
