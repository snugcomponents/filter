<?php

declare(strict_types=1);

namespace Snugcomponents\Filter\Examples\Builders;

use Nette\SmartObject;
use Snugcomponents\Filter\FilterDataBuilder;

use function array_filter;

class ArrayDataBuilder implements FilterDataBuilder
{
    use SmartObject;

    /** @var array{age: int|null}|null */
    private array|null $filter = null;

    /**
     * @param array<int, array<string, mixed>> $data
     */
    public function __construct(private array $data)
    {
    }

    /**
     * Filter called by the extension
     *
     * @param array<mixed>|float|int|string|null ...$filterData
     */
    public function filter(array|float|int|string|null ...$filterData): static
    {
        // phpcs:ignore SlevomatCodingStandard.Commenting.InlineDocCommentDeclaration.MissingVariable
        /** @var array{age: int|null} $filterData */
        $this->filter = $filterData;

        return $this;
    }

    /**
     * Build method should be used in all builders.
     * This one applies the filter and returns the data.
     *
     * Remember to call the filtering method and other builder methods in the build method.
     * This provides you flexibility when you need to change settings, and it is lazy,
     * so when you don't call the build method, then filtering will not execute.
     *
     * @return array<int, array<string, mixed>>
     */
    public function build(): array
    {
        if ($this->filter) {
            $this->innerFilter(...$this->filter); // Because of checking data types.
        }

        return $this->data;
    }

    /**
     * All parameters need to be always nullable. It is for filtering only some values...
     * Extension will always pass all the arguments you specified in FilterSetupBuilder
     */
    private function innerFilter(int|null $age): void
    {
        if ($age === null) {
            return;
        }

        $this->data = array_filter($this->data, static fn (array $person): bool => $person['age'] === $age);
    }
}
