<?php

declare(strict_types=1);

namespace Snugcomponents\Filter;

use Nette\Application\UI\Form;
use Nette\Schema\Elements\Structure;
use Nette\Schema\Expect;
use Nette\SmartObject;
use Stringable;

use function array_keys;
use function assert;
use function is_scalar;
use function is_string;

class FilterSetupBuilder
{
    use SmartObject;

    /** @var array<FilterSetup> */
    private array $setups = [];

    final public function __construct()
    {
        // Empty constructor because of create method and its new static() call
    }

    public static function create(): static
    {
        return new static();
    }

    public function addText(string $name, string $caption): static
    {
        $this->setups[$name] = new FilterSetup(
            caption: $caption,
            type: InputType::Text,
        );

        return $this;
    }

    public function addInt(string $name, string $caption): static
    {
        $this->setups[$name] = new FilterSetup(
            caption: $caption,
            type: InputType::Int,
        );

        return $this;
    }

    public function addFloat(string $name, string $caption): static
    {
        $this->setups[$name] = new FilterSetup(
            caption: $caption,
            type: InputType::Float,
        );

        return $this;
    }

    public function addDatetime(string $name, string $caption): static
    {
        $this->setups[$name] = new FilterSetup(
            caption: $caption,
            type: InputType::Datetime,
        );

        return $this;
    }

    /**
     * @param array<int|string, scalar> $items
     * @throws Exception\Exception
     */
    public function addSelect(string $name, string $caption, array $items = []): static
    {
        $this->setups[$name] = new FilterSetupChoice(
            caption: $caption,
            type: InputType::Select,
            items: $items,
        );

        return $this;
    }

    /**
     * @param array<int|string, scalar> $items
     * @throws Exception\Exception
     */
    public function addCheckboxList(string $name, string $caption, array $items = []): static
    {
        $this->setups[$name] = new FilterSetupChoice(
            caption: $caption,
            type: InputType::CheckboxList,
            items: $items,
        );

        return $this;
    }

    /**
     * @param array<int|string, scalar> $items
     * @throws Exception\Exception
     */
    public function addRadioList(string $name, string $caption, array $items = []): static
    {
        $this->setups[$name] = new FilterSetupChoice(
            caption: $caption,
            type: InputType::RadioList,
            items: $items,
        );

        return $this;
    }

    public function buildForm(Form $form): Form
    {
        foreach ($this->setups as $filterName => $filterSetup) {
            /** @noinspection PhpPossiblePolymorphicInvocationInspection */
            match ($filterSetup->type) {
                InputType::Text => $form->addText($filterName, $filterSetup->caption)->setNullable(),
                InputType::Int => $form->addInteger($filterName, $filterSetup->caption)->setNullable(),
                InputType::Float => $form->addText($filterName, $filterSetup->caption)->setNullable()->addRule(
                    $form::FLOAT,
                )->setHtmlType('float'),
                InputType::Datetime => $form->addText($filterName, $filterSetup->caption)->setNullable()->setHtmlType(
                    'datetime',
                ),
                InputType::Select => $form->addSelect(
                    $filterName,
                    $filterSetup->caption,
                    $this->getItemsFromChoice($filterSetup),
                )->setPrompt(''),
                InputType::CheckboxList => $form->addCheckboxList(
                    $filterName,
                    $filterSetup->caption,
                    $this->getItemsFromChoice($filterSetup),
                ),
                InputType::RadioList => $form->addRadioList(
                    $filterName,
                    $filterSetup->caption,
                    $this->getItemsFromChoice($filterSetup),
                ),
                InputType::MultiSelect => $form->addMultiSelect(
                    $filterName,
                    $filterSetup->caption,
                    $this->getItemsFromChoice($filterSetup),
                ),
            };
        }

        return $form;
    }

    public function getSchema(): Structure
    {
        $schemaArray = [];

        foreach ($this->setups as $filterName => $filterSetup) {
            match ($filterSetup->type) {
                InputType::Text, InputType::Datetime => $schemaArray[$filterName] = Expect::string()->nullable(),
                InputType::Int => $schemaArray[$filterName] = Expect::int()->nullable(),
                InputType::Float => $schemaArray[$filterName] = Expect::float()->nullable(),
                InputType::Select, InputType::RadioList => $schemaArray[$filterName] = Expect::anyOf(
                    ...$this->translateAnyOfArrayToArrayWithTheSameValuesButAllOfThemAlsoInStringBecauseOfNettePersistentWrongFunction( // phpcs:ignore
                        array_keys($this->getItemsFromChoice($filterSetup)),
                    ),
                )->nullable(),
                InputType::CheckboxList, InputType::MultiSelect => $schemaArray[$filterName] = Expect::arrayOf(
                    Expect::anyOf(
                        ...$this->translateAnyOfArrayToArrayWithTheSameValuesButAllOfThemAlsoInStringBecauseOfNettePersistentWrongFunction( // phpcs:ignore
                            array_keys($this->getItemsFromChoice($filterSetup)),
                        ),
                    )->nullable(),
                )->nullable(),
            };
        }

        return Expect::structure($schemaArray)
            ->castTo('array');
    }

    /**
     * @return array<int|string, scalar|Stringable>
     */
    private function getItemsFromChoice(FilterSetup $filterSetupChoice): array
    {
        assert($filterSetupChoice instanceof FilterSetupChoice);

        return $filterSetupChoice->items;
    }

    /**
     * TODO: Remove after that Nette implements url persistent parameters translation
     * to integers, booleans, floats etc...
     *
     * @param array<mixed> $original
     * @return array<mixed>
     */
    private function translateAnyOfArrayToArrayWithTheSameValuesButAllOfThemAlsoInStringBecauseOfNettePersistentWrongFunction( // phpcs:ignore
        array $original,
    ): array {
        $new = $original;

        foreach ($original as $value) {
            if (is_string($value) || !is_scalar($value)) {
                continue;
            }

            $new[] = (string) $value;
        }

        return $new;
    }
}
