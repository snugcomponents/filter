<?php

declare(strict_types=1);

namespace Snugcomponents\Filter;

use Snugcomponents\Filter\Exception\Exception;
use Snugcomponents\Filter\Exception\ExceptionCase;
use Stringable;

use function gettype;
use function is_scalar;

class FilterSetupChoice extends FilterSetup
{
    /**
     * @param array<int|string, scalar|Stringable> $items
     * @throws Exception
     */
    public function __construct(
        string $caption,
        InputType $type,
        public array $items,
    ) {
        foreach ($items as $key => $item) {
            if (!is_scalar($item) && !($item instanceof Stringable)) { // @phpstan-ignore-line
                throw new Exception(ExceptionCase::InvalidArgumentScalar, gettype($key));
            }
        }

        parent::__construct($caption, $type);
    }
}
