<?php

declare(strict_types=1);

namespace Snugcomponents\Filter;

use Nette\SmartObject;

class FilterSetup
{
    use SmartObject;

    public function __construct(public string $caption, public InputType $type)
    {
    }
}
