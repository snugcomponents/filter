<?php

declare(strict_types=1);

namespace Snugcomponents\Filter;

use Nette\Application\UI\Form;

interface FormFactory
{
    public function create(): Form;
}
