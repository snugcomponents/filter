<?php

declare(strict_types=1);

namespace Snugcomponents\Filter;

use Nette\Application\UI\Form;
use Nette\Localization\Translator;

class BaseFormFactory implements FormFactory
{
    public function __construct(
        private readonly Translator|null $translator = null,
    ) {
    }

    public function create(): Form
    {
        $form = new Form();
        $form->getElementPrototype()->setAttribute('novalidate', 'novalidate');
        $form->setTranslator($this->translator);
        return $form;
    }
}
